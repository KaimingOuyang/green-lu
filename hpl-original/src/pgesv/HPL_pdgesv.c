/*
 * -- High Performance Computing Linpack Benchmark (HPL)
 *    HPL - 2.2 - February 24, 2016
 *    Antoine P. Petitet
 *    University of Tennessee, Knoxville
 *    Innovative Computing Laboratory
 *    (C) Copyright 2000-2008 All Rights Reserved
 *
 * -- Copyright notice and Licensing terms:
 *
 * Redistribution  and  use in  source and binary forms, with or without
 * modification, are  permitted provided  that the following  conditions
 * are met:
 *
 * 1. Redistributions  of  source  code  must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce  the above copyright
 * notice, this list of conditions,  and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. All  advertising  materials  mentioning  features  or  use of this
 * software must display the following acknowledgement:
 * This  product  includes  software  developed  at  the  University  of
 * Tennessee, Knoxville, Innovative Computing Laboratory.
 *
 * 4. The name of the  University,  the name of the  Laboratory,  or the
 * names  of  its  contributors  may  not  be used to endorse or promote
 * products  derived   from   this  software  without  specific  written
 * permission.
 *
 * -- Disclaimer:
 *
 * THIS  SOFTWARE  IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,  INCLUDING,  BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY
 * OR  CONTRIBUTORS  BE  LIABLE FOR ANY  DIRECT,  INDIRECT,  INCIDENTAL,
 * SPECIAL,  EXEMPLARY,  OR  CONSEQUENTIAL DAMAGES  (INCLUDING,  BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA OR PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------
 */
/*
 * Include files
 */
#include "hpl.h"

double timegesv;
extern double cvalues[2];
extern int eventset;

int initNativeEventSet() {
	int retval, cid, rapl_cid = -1, numcmp;
	int EventSet = PAPI_NULL;
	// long long *values;
	int num_events = 0;
	int code;
	// char event_names[MAX_RAPL_EVENTS][PAPI_MAX_STR_LEN];
	// char units[MAX_RAPL_EVENTS][PAPI_MIN_STR_LEN];
	// int data_type[MAX_RAPL_EVENTS];
	int r, i;
	const PAPI_component_info_t *cmpinfo = NULL;
	PAPI_event_info_t evinfo;


	retval = PAPI_library_init( PAPI_VER_CURRENT );
	if ( retval != PAPI_VER_CURRENT ) {
		printf("%s: PAPI library init error [line %s].\n", __FUNCTION__, __LINE__);
		return -1;
	}

	// numcmp = PAPI_num_components();

	// for (cid = 0; cid < numcmp; cid++) {
	// 	if ( (cmpinfo = PAPI_get_component_info(cid)) == NULL) {
	// 		printf("%s: PAPI_get_component_info failed [line %s].\n", __FILE__, __LINE__);
	// 		return -1;
	// 	}

	// 	if (strstr(cmpinfo->name, "rapl")) {
	// 		rapl_cid = cid;
	// 		//printf("Found rapl component at cid %d.\n", rapl_cid);

	// 		if (cmpinfo->disabled) {
	// 			printf("RAPL component disabled: %s [SKIP].\n", cmpinfo->disabled_reason);
	// 			return -1;
	// 		}
	// 		break;
	// 	}
	// }

	// /* Component not found */
	// if (cid == numcmp) {
	// 	printf("%s: No rapl component found [line %s].\n", __FILE__, __LINE__);
	// 	return -1;
	// }

	/* Create EventSet */
	retval = PAPI_create_eventset( &EventSet );
	if (retval != PAPI_OK) {
		printf("%s: PAPI_create_eventset() [line %s].", __FILE__, __LINE__);
		return -1;
	}

	/* Add all events */

	code = PAPI_NATIVE_MASK;

	// r = PAPI_enum_cmp_event( &code, PAPI_ENUM_FIRST, rapl_cid );
	retval = PAPI_event_name_to_code("rapl:::PACKAGE_ENERGY:PACKAGE0", &code);
	if ( retval != PAPI_OK ) {
		printf("Error translating %#x\n", code);
		return -1;
	}
	retval = PAPI_add_event( EventSet, code );
	if ( retval != PAPI_OK ) {
		printf("Error adding rapl:::PACKAGE_ENERGY:PACKAGE0\n");
		return -1;
	}
	retval = PAPI_event_name_to_code("rapl:::PACKAGE_ENERGY:PACKAGE1", &code);
	if ( retval != PAPI_OK ) {
		printf("Error translating %#x\n", code);
		return -1;
	}
	retval = PAPI_add_event( EventSet, code );
	if ( retval != PAPI_OK ) {
		printf("Error adding rapl:::PACKAGE_ENERGY:PACKAGE1\n");
		return -1;
	}
	return EventSet;
}


void outputEnergyIndices(char event_names[][PAPI_MAX_STR_LEN], char units[][PAPI_MIN_STR_LEN], int* data_type, long long* values, int num_events, double elapsed_time, char* file) {

	int i;
	FILE* fp = fopen(file, "w");
	fprintf(fp, "\nStopping measurements, took %.3fs, gathering results...\n\n",
	        elapsed_time);

	fprintf(fp, "Scaled energy measurements:\n");

	for (i = 0; i < num_events; i++) {
		if (strstr(units[i], "nJ")) {

			fprintf(fp, "%-40s%12.6f J\t(Average Power %.1fW)\n",
			        event_names[i],
			        (double)values[i] / 1.0e9,
			        ((double)values[i] / 1.0e9) / elapsed_time);
		}
	}

	// fprintf(fp, "\n");
	// fprintf(fp, "Energy measurement counts:\n");

	// for (i = 0; i < num_events; i++) {
	//    if (strstr(event_names[i], "ENERGY_CNT")) {
	//       fprintf(fp, "%-40s%12lld\t%#08llx\n", event_names[i], values[i], values[i]);
	//    }
	// }

	// fprintf(fp, "\n");
	// fprintf(fp, "Scaled Fixed values:\n");

	// for (i = 0; i < num_events; i++) {
	//    if (!strstr(event_names[i], "ENERGY")) {
	//       if (data_type[i] == PAPI_DATATYPE_FP64) {

	//          union {
	//             long long ll;
	//             double fp;
	//          } result;

	//          result.ll = values[i];
	//          fprintf(fp, "%-40s%12.3f %s\n", event_names[i], result.fp, units[i]);
	//       }
	//    }
	// }

	// fprintf(fp, "\n");
	// fprintf(fp, "Fixed value counts:\n");

	// for (i = 0; i < num_events; i++) {
	//    if (!strstr(event_names[i], "ENERGY")) {
	//       if (data_type[i] == PAPI_DATATYPE_UINT64) {
	//          fprintf(fp, "%-40s%12lld\t%#08llx\n", event_names[i], values[i], values[i]);
	//       }
	//    }
	// }
	fclose(fp);
	return;
}


void finalizeEventset(int EventSet) {
	/* Done, clean up */
	int retval;
	retval = PAPI_cleanup_eventset( EventSet );
	if (retval != PAPI_OK) {
		printf("PAPI_cleanup_eventset()");
		exit(1);
	}

	retval = PAPI_destroy_eventset( &EventSet );
	if (retval != PAPI_OK) {
		printf("API_destroy_eventset()");
		exit(1);
	}
	return;
}

#ifdef STDC_HEADERS
void HPL_pdgesv
(
    HPL_T_grid *                     GRID,
    HPL_T_palg *                     ALGO,
    HPL_T_pmat *                     A
)
#else
void HPL_pdgesv
( GRID, ALGO, A )
HPL_T_grid *                     GRID;
HPL_T_palg *                     ALGO;
HPL_T_pmat *                     A;
#endif
{
	/*
	 * Purpose
	 * =======
	 *
	 * HPL_pdgesv factors a N+1-by-N matrix using LU factorization with row
	 * partial pivoting.  The main algorithm  is the "right looking" variant
	 * with  or  without look-ahead.  The  lower  triangular  factor is left
	 * unpivoted and the pivots are not returned. The right hand side is the
	 * N+1 column of the coefficient matrix.
	 *
	 * Arguments
	 * =========
	 *
	 * GRID    (local input)                 HPL_T_grid *
	 *         On entry,  GRID  points  to the data structure containing the
	 *         process grid information.
	 *
	 * ALGO    (global input)                HPL_T_palg *
	 *         On entry,  ALGO  points to  the data structure containing the
	 *         algorithmic parameters.
	 *
	 * A       (local input/output)          HPL_T_pmat *
	 *         On entry, A points to the data structure containing the local
	 *         array information.
	 *
	 * ---------------------------------------------------------------------
	 */
	/* ..
	 * .. Executable Statements ..
	 */
	if ( A->n <= 0 ) return;

	A->info = 0;
	// char fileName[64];
	// sprintf(fileName, "energy_%d_%d", A->n, A->nb);
	// char event_names[MAX_RAPL_EVENTS][PAPI_MAX_STR_LEN];
	// char units[MAX_RAPL_EVENTS][PAPI_MIN_STR_LEN];
	// int data_type[MAX_RAPL_EVENTS];
	// int num_events;
	// int retval;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	// int eventset = initNativeEventSet(event_names, units, data_type, &num_events);
	// long long* values = calloc(num_events, sizeof(long long));
	// memset(values, 0, sizeof(long long) * num_events);

	// /* Start Counting */

	// if (rank < 3) {
	// 	retval = PAPI_start(eventset);
	// }
	
	int retval;
	int num_events;
	eventset = initNativeEventSet();
	//long long* values = calloc(num_events, sizeof(long long));
	//memset(values, 0, sizeof(long long) * num_events);
	long long values[2];
	/* Start Counting */
	if (rank % CORE == 0) {
		retval = PAPI_start(eventset);
		settimer(300, 0);
		if (retval != PAPI_OK) {
			printf("PAPI_start() fails\n");
			exit(1);
		}
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	timegesv = HPL_timer_walltime();
	if ( ( ALGO->depth == 0 ) || ( GRID->npcol == 1 ) ) {
		HPL_pdgesv0(  GRID, ALGO, A );
	} else {
		HPL_pdgesvK2( GRID, ALGO, A );
	}
	MPI_Barrier(MPI_COMM_WORLD);
	timegesv = HPL_timer_walltime() - timegesv;
	/* Stop Counting */
	if (rank % CORE == 0) {
		settimer(0, 0);
		retval = PAPI_stop(eventset, values);

		cvalues[0] +=  values[0] / 1.0e9;
		cvalues[1] +=  values[1] / 1.0e9;
		// system("hostname");
		// int i;
		// for (i = 0; i < num_events; i++) {
		// 	if (strstr(units[i], "nJ")) {
		// 		printf("%40s %lf\n" ,  event_names[i], values[i] / 1.0e9);
		// 	}
		// }
		// printf("(%lf J, %lf W, %lf s)\n", energy, energy / wtime[0] / size, wtime[0]);
		if (retval != PAPI_OK) {
			printf("PAPI_stop() fails\n");
			exit(1);
		}
	}
	// time = HPL_timer_walltime() - time;
	// if (rank < 3) {
	// 	retval = PAPI_stop(eventset, values);
	// 	// system("hostname");
	// 	// int i;
	// 	// for (i = 0; i < num_events; i++) {
	// 	//    if (strstr(units[i], "nJ")) {
	// 	//       printf("%40s %lf\n" ,  event_names[i], values[i] / 1.0e9);
	// 	//    }
	// 	// }
	// 	// printf("(%lf J, %lf W, %lf s)\n", energy, energy / wtime[0] / size, wtime[0]);
	// }
	// if (retval != PAPI_OK) {
	//    printf("PAPI_stop() fails\n");
	//    exit(1);
	// }
	// long long* glbValues = calloc(num_events, sizeof(long long));
	// double glbTime;
	// MPI_Reduce(values, glbValues, num_events, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	// MPI_Reduce(&time, &glbTime, num_events, MPI_LONG_LONG, MPI_MAX, 0, MPI_COMM_WORLD);

	// int size;
	// MPI_Comm_size(MPI_COMM_WORLD, &size);
	// if (rank == 0) {
	// 	double energy = 0.0;
	// 	int i;
	// 	for (i = 0; i < num_events; i++) {
	// 		if (strstr(event_names[i], "rapl:::PACKAGE_ENERGY:PACKAGE0") || strstr(event_names[i], "rapl:::PACKAGE_ENERGY:PACKAGE1")) {
	// 			// printf("glbValues[i] = %lld\n", glbValues[i]);
	// 			energy += (double) glbValues[i] / 1.0e9;
	// 		}
	// 	}
	// 	printf("(%lf J, %lf W, %lf s)\n", energy, energy / glbTime / size, glbTime);
	// 	//outputEnergyIndices(event_names, units, data_type, glbValues, num_events, wtime[0], fileName);
	// }
	// MPI_Finalize();
	// exit(0);
	/*
	 * Solve upper triangular system
	 */
	if ( A->info == 0 ) HPL_pdtrsv( GRID, A );
	/*
	 * End of HPL_pdgesv
	 */
}
