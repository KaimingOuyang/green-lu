/*
 * -- High Performance Computing Linpack Benchmark (HPL)
 *    HPL - 2.2 - February 24, 2016
 *    Antoine P. Petitet
 *    University of Tennessee, Knoxville
 *    Innovative Computing Laboratory
 *    (C) Copyright 2000-2008 All Rights Reserved
 *
 * -- Copyright notice and Licensing terms:
 *
 * Redistribution  and  use in  source and binary forms, with or without
 * modification, are  permitted provided  that the following  conditions
 * are met:
 *
 * 1. Redistributions  of  source  code  must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce  the above copyright
 * notice, this list of conditions,  and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. All  advertising  materials  mentioning  features  or  use of this
 * software must display the following acknowledgement:
 * This  product  includes  software  developed  at  the  University  of
 * Tennessee, Knoxville, Innovative Computing Laboratory.
 *
 * 4. The name of the  University,  the name of the  Laboratory,  or the
 * names  of  its  contributors  may  not  be used to endorse or promote
 * products  derived   from   this  software  without  specific  written
 * permission.
 *
 * -- Disclaimer:
 *
 * THIS  SOFTWARE  IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,  INCLUDING,  BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY
 * OR  CONTRIBUTORS  BE  LIABLE FOR ANY  DIRECT,  INDIRECT,  INCIDENTAL,
 * SPECIAL,  EXEMPLARY,  OR  CONSEQUENTIAL DAMAGES  (INCLUDING,  BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA OR PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT,  STRICT LIABILITY,  OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ---------------------------------------------------------------------
 */
/*
 * Include files
 */
#include "hpl.h"
char fileName[128];
double comptime;
#ifdef STDC_HEADERS
void HPL_pdgesv0
(
    HPL_T_grid *                     GRID,
    HPL_T_palg *                     ALGO,
    HPL_T_pmat *                     A
)
#else
void HPL_pdgesv0
( GRID, ALGO, A )
HPL_T_grid *                     GRID;
HPL_T_palg *                     ALGO;
HPL_T_pmat *                     A;
#endif
{
	/*
	 * Purpose
	 * =======
	 *
	 * HPL_pdgesv0 factors a N+1-by-N matrix using LU factorization with row
	 * partial pivoting.  The main algorithm  is the "right looking" variant
	 * without look-ahead. The lower triangular factor is left unpivoted and
	 * the pivots are not returned. The right hand side is the N+1 column of
	 * the coefficient matrix.
	 *
	 * Arguments
	 * =========
	 *
	 * GRID    (local input)                 HPL_T_grid *
	 *         On entry,  GRID  points  to the data structure containing the
	 *         process grid information.
	 *
	 * ALGO    (global input)                HPL_T_palg *
	 *         On entry,  ALGO  points to  the data structure containing the
	 *         algorithmic parameters.
	 *
	 * A       (local input/output)          HPL_T_pmat *
	 *         On entry, A points to the data structure containing the local
	 *         array information.
	 *
	 * ---------------------------------------------------------------------
	 */
	/*
	 * .. Local Variables ..
	 */
	HPL_T_panel                * * panel = NULL;
	HPL_T_UPD_FUN              HPL_pdupdate;
	int                        N, j, jb, n, nb, tag = MSGID_BEGIN_FACT,
	                                            test = HPL_KEEP_TESTING;
#ifdef HPL_PROGRESS_REPORT
	double start_time, time, gflops;
#endif
	/* ..
	 * .. Executable Statements ..
	 */
	if ( ( N = A->n ) <= 0 ) return;

#ifdef HPL_PROGRESS_REPORT
	start_time = HPL_timer_walltime();
#endif

	HPL_pdupdate = ALGO->upfun; nb = A->nb;
	/*
	 * Allocate a panel list of length 1 - Allocate panel[0] resources
	 */
	panel = (HPL_T_panel **)malloc( sizeof( HPL_T_panel * ) );
	if ( panel == NULL )
	{ HPL_pabort( __LINE__, "HPL_pdgesv0", "Memory allocation failed" ); }

	HPL_pdpanel_new( GRID, ALGO, N, N + 1, Mmin( N, nb ), A, 0, 0, tag,
	                 &panel[0] );
	/*
	 * Loop over the columns of A
	 */
	int rank;
	// double time;
	// double ptime;
	// double utime;
	// double btime;
	// double* timearray = (double*) malloc(((N / nb) + 3) * sizeof(double));
	// double* comparray = (double*) malloc(((N / nb) + 3) * sizeof(double));
	// double* pdarray = (double*) malloc(((N / nb) + 3) * sizeof(double));
	// double* tlarray = (double*) malloc(((N / nb) + 3) * sizeof(double));
	// double* bcarray = (double*) malloc(((N / nb) + 3) * sizeof(double));
	// int itert = 0;
	// int iterp = 0;
	// int iterc = 0;
	// int itertl = 0;
	// int iterbc = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	// sprintf(fileName, "%d-%d-%d-%d.%d", A->n, A->nb, GRID->nprow, GRID->npcol, rank);
	for ( j = 0; j < N; j += nb ) {
		n = N - j; jb = Mmin( n, nb );
		if ( GRID->myrow == 0 && GRID->mycol == 0 && j > 0 ) {
			printf("Iteration: %d, total: %d\n", j / nb, N / nb);
			fflush(stdout);
		}
#ifdef HPL_PROGRESS_REPORT
		/* if this is process 0,0 and not the first panel */
		if ( GRID->myrow == 0 && GRID->mycol == 0 && j > 0 ) {
			time = HPL_timer_walltime() - start_time;
			gflops = 2.0 * (N * (double)N * N - n * (double)n * n) / 3.0 / (time > 0.0 ? time : 1e-6) / 1e9;
			HPL_fprintf( stdout, "Column=%09d Fraction=%4.1f%% Gflops=%9.3e\n", j, j * 100.0 / N, gflops);
		}
#endif
		/*
		 * Release panel resources - re-initialize panel data structure
		 */
		(void) HPL_pdpanel_free( panel[0] );
		HPL_pdpanel_init( GRID, ALGO, n, n + 1, jb, A, j, j, tag, panel[0] );
		/*
		 * Factor and broadcast current panel - update
		 */
		// ptime = MPI_Wtime();
		HPL_pdfact(               panel[0] );
		// ptime = MPI_Wtime() - ptime;

		// pdarray[iterp++] = ptime;


		(void) HPL_binit(         panel[0] );
		// if (j == 0)
		// 	MPI_Barrier(MPI_COMM_WORLD);
		// btime = MPI_Wtime();
		do
		{ (void) HPL_bcast(       panel[0], &test ); }
		while ( test != HPL_SUCCESS );
		// btime = MPI_Wtime() - btime;
		(void) HPL_bwait(         panel[0] );
		// bcarray[iterbc++] = btime;
		// if (j > 0) {
		// 	time = MPI_Wtime() - time;
		// 	tlarray[itertl++] = time;
		// }


		// if (j / nb == 10)
		// 	break;

		// time = MPI_Wtime();
		// utime = MPI_Wtime();
		HPL_pdupdate( NULL, NULL, panel[0], -1 );
		// utime = MPI_Wtime() - utime;
		// timearray[itert++] = utime;
		// comparray[iterc++] = comptime;
		// if (panel[0]->grid->mycol == panel[0]->pcol) {
		// 	printf("rank: %d, ptime: %lf, utime: %lf\n", rank, ptime, utime);
		// }

		/*
		 * Update message id for next factorization
		 */
		tag = MNxtMgid( tag, MSGID_BEGIN_FACT, MSGID_END_FACT );
	}
	// char pdfile[64];
	// char updatefile[64];
	// char compfile[64];
	// char totalfile[64];
	// char bcfile[64];
	// sprintf(pdfile, "pd_%d_%d_%d_%d", A->n, A->nb, GRID->nprow, GRID->npcol);
	// sprintf(updatefile, "update_%d_%d_%d_%d", A->n, A->nb, GRID->nprow, GRID->npcol);
	// sprintf(compfile, "comp_%d_%d_%d_%d", A->n, A->nb, GRID->nprow, GRID->npcol);
	// sprintf(totalfile, "total_%d_%d_%d_%d", A->n, A->nb, GRID->nprow, GRID->npcol);
	// sprintf(bcfile, "bc_%d_%d_%d_%d", A->n, A->nb, GRID->nprow, GRID->npcol);

	// // kaiming
	// // int glbiter = (N / nb) + ((N % nb) == 0 ? 0 : 1), size;
	// int glbiter = 9, size;
	// int i, flag;
	// int mycol = GRID->mycol;
	// int scol = 0;

	// MPI_Comm_size(MPI_COMM_WORLD, &size);
	// // MPI_Allreduce(&iter, &glbiter, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
	// for (i = 0; i < glbiter; ++i) {
	// 	if (mycol == scol) {
	// 		int rrank;
	// 		MPI_Comm_rank(GRID->col_comm, &rrank);
	// 		if (rrank == 0) {
	// 			FILE* fp = fopen(pdfile, "a");
	// 			fprintf(fp, "%d:%lf ", rank, pdarray[i]);
	// 			fclose(fp);
	// 			MPI_Send(&flag, 1, MPI_INT, rrank + 1, 0, GRID->col_comm);
	// 		} else {
	// 			MPI_Status stat;
	// 			MPI_Recv(&flag, 1, MPI_INT, rrank - 1, 0, GRID->col_comm, &stat);

	// 			FILE* fp = fopen(pdfile, "a");
	// 			fprintf(fp, "%d:%lf ", rank, pdarray[i]);
	// 			if (rrank == GRID->nprow - 1) fprintf(fp, "\n");
	// 			fclose(fp);
	// 			if (rrank != GRID->nprow - 1)
	// 				MPI_Send(&flag, 1, MPI_INT, rrank + 1, 0, GRID->col_comm);
	// 		}
	// 	}


	// 	if (rank == 0) {
	// 		// FILE* fp1 = fopen(updatefile, "a");
	// 		// FILE* fp2 = fopen(compfile, "a");
	// 		// FILE* fp3 = fopen(totalfile, "a");
	// 		FILE* fp4 = fopen(bcfile, "a");
	// 		// fprintf(fp1, "%d:%lf ", rank, timearray[i]);
	// 		// fprintf(fp2, "%d:%lf ", rank, comparray[i]);
	// 		// fprintf(fp3, "%d:%lf ", rank, tlarray[i]);
	// 		fprintf(fp4, "%d:%lf ", rank, bcarray[i]);
	// 		// fclose(fp1);
	// 		// fclose(fp2);
	// 		// fclose(fp3);
	// 		fclose(fp4);
	// 		MPI_Send(&flag, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
	// 	} else {
	// 		MPI_Status stat;
	// 		MPI_Recv(&flag, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &stat);

	// 		// FILE* fp1 = fopen(updatefile, "a");
	// 		// FILE* fp2 = fopen(compfile, "a");
	// 		// FILE* fp3 = fopen(totalfile, "a");
	// 		FILE* fp4 = fopen(bcfile, "a");
	// 		// fprintf(fp1, "%d:%lf ", rank, timearray[i]);
	// 		// fprintf(fp2, "%d:%lf ", rank, comparray[i]);
	// 		// fprintf(fp3, "%d:%lf ", rank, tlarray[i]);
	// 		fprintf(fp4, "%d:%lf ", rank, bcarray[i]);
	// 		if (rank == size - 1) {
	// 			// fprintf(fp1, "\n");
	// 			// fprintf(fp2, "\n");
	// 			// fprintf(fp3, "\n");
	// 			fprintf(fp4, "\n");
	// 		}
	// 		// fclose(fp1);
	// 		// fclose(fp2);
	// 		// fclose(fp3);
	// 		fclose(fp4);

	// 		if (rank != size - 1)
	// 			MPI_Send(&flag, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
	// 	}
	// 	scol = MModAdd1(scol, GRID->npcol);
	// 	MPI_Barrier(MPI_COMM_WORLD);
	// }

	/*
	 * Release panel resources and panel list
	 */
	(void) HPL_pdpanel_disp( &panel[0] );

	if ( panel ) free( panel );
	/*
	 * End of HPL_pdgesv0
	 */
}
